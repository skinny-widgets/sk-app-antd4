
import { AntdSkApp }  from '../../sk-app-antd/src/antd-sk-app.js';

export class Antd4SkApp extends AntdSkApp {

    get prefix() {
        return 'antd4';
    }

}
